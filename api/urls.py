from django.conf.urls import url, include
from rest_framework import routers

from api.views import FooterViewSet, PageViewSet, NavigationViewSet, LinkViewSet, ActionViewSet

router = routers.DefaultRouter()
router.register(r'page', PageViewSet)
router.register(r'navigation', NavigationViewSet)
router.register(r'link', LinkViewSet)
router.register(r'action', ActionViewSet)
router.register(r'footer', FooterViewSet)

urlpatterns = [
    url(r'^', include(router.urls))
]