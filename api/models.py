from __future__ import unicode_literals

from django.db import models
from django.db.models.fields import CharField, TextField, URLField, BooleanField, IntegerField
from django.db.models.fields.files import ImageField
from django.db.models.fields.related import OneToOneField, ForeignKey
from imagekit.models.fields import ImageSpecField
from pilkit.processors.resize import ResizeToFill


class Page(models.Model):
    title = CharField(max_length=255, default='', blank=False)
    lang = CharField(max_length=4, default='', blank=True)
    charset = CharField(max_length=8, default='', blank=True)
    description = TextField(max_length=1024*1024, default='', blank=True)
    path = CharField(max_length=255, default='', blank=True)
    isDefault = BooleanField(default=False)

    icon = ImageField(upload_to='icons')
    icon16 = ImageSpecField(source='icon',
                            processors=[ResizeToFill(16, 16)],
                            format='PNG',
                            options={'quality': 60})
    icon32 = ImageSpecField(source='icon',
                            processors=[ResizeToFill(32, 32)],
                            format='PNG',
                            options={'quality': 60})
    icon192 = ImageSpecField(source='icon',
                            processors=[ResizeToFill(192, 192)],
                            format='PNG',
                            options={'quality': 60})


class Navigation(models.Model):
    page = OneToOneField(Page, on_delete=models.CASCADE, related_name='navigation')
    title = CharField(max_length=255, default='', blank=False)

    icon = models.ImageField(upload_to='icons')
    icon192 = ImageSpecField(source='icon',
                             processors=[ResizeToFill(192, 192)],
                             format='PNG',
                             options={'quality': 60})


class Link(models.Model):
    navigation = ForeignKey(Navigation, on_delete=models.CASCADE, related_name='links')
    text = CharField(max_length=255, default='', blank=False)
    tooltip = TextField(max_length=1024*1024, default='', blank=True)
    index = IntegerField(default=0)
    isToPage = BooleanField(default=False)
    page = ForeignKey(Page, on_delete=models.SET_NULL, null=True)
    url = URLField(default='', blank=True)


class Action(models.Model):
    navigation = ForeignKey(Navigation, on_delete=models.CASCADE, related_name='actions')
    text = CharField(max_length=255, default='', blank=False)
    tooltip = TextField(max_length=1024*1024, default='', blank=True)
    index = IntegerField(default=0)
    isToPage = BooleanField(default=False)
    page = ForeignKey(Page, on_delete=models.SET_NULL, null=True)
    url = URLField(default='', blank=True)


class Footer(models.Model):
    page = OneToOneField(Page, on_delete=models.CASCADE, related_name='footer')
    title = CharField(max_length=255, default='', blank=False)
    text = CharField(max_length=1024*1024, default='', blank=False)
    copyright = CharField(max_length=255, default='', blank=False)
    isToPage = BooleanField(default=False)
    page = ForeignKey(Page, on_delete=models.SET_NULL, null=True)
    url = URLField(default='', blank=True)



