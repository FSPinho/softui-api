from rest_framework.fields import ImageField
from rest_framework.serializers import HyperlinkedModelSerializer

from api.models import Page, Navigation, Link, Action, Footer


class PageSerializer(HyperlinkedModelSerializer):
    icon16 = ImageField(read_only=True)
    icon32 = ImageField(read_only=True)
    icon192 = ImageField(read_only=True)

    class Meta:
        model = Page
        fields = '__all__'


class NavigationSerializer(HyperlinkedModelSerializer):
    class Meta:
        model = Navigation
        fields = '__all__'


class LinkSerializer(HyperlinkedModelSerializer):
    class Meta:
        model = Link
        fields = '__all__'


class ActionSerializer(HyperlinkedModelSerializer):
    class Meta:
        model = Action
        fields = '__all__'


class FooterSerializer(HyperlinkedModelSerializer):
    class Meta:
        model = Footer
        fields = '__all__'