from django.shortcuts import render
from rest_framework.viewsets import ModelViewSet

from api.models import Page, Navigation, Link, Action, Footer
from api.serializers import PageSerializer, NavigationSerializer, LinkSerializer, ActionSerializer, FooterSerializer


class PageViewSet(ModelViewSet):
    queryset = Page.objects.all()
    serializer_class = PageSerializer


class NavigationViewSet(ModelViewSet):
    queryset = Navigation.objects.all()
    serializer_class = NavigationSerializer


class LinkViewSet(ModelViewSet):
    queryset = Link.objects.all()
    serializer_class = LinkSerializer


class ActionViewSet(ModelViewSet):
    queryset = Action.objects.all()
    serializer_class = ActionSerializer


class FooterViewSet(ModelViewSet):
    queryset = Footer.objects.all()
    serializer_class = FooterSerializer
